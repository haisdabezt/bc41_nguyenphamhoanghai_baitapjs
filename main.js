// Câu 1
let xuatBang = () => {
  for (var i = 1; i <= 10; i++) {
    var string = "";
    for (var j = 1; j <= 10; j++) {
      string += (i - 1) * 10 + j + " ";
    }
    console.log(string);
  }
};
console.log("Câu 1 : In bảng ");
xuatBang();
// Câu 2
let mangSNT = (mang) => {
  let checkSNT = (number) => {
    if (number <= 1) {
      return false;
    }

    for (var i = 2; i < number; i++) {
      if (number % i === 0) {
        return false;
      }
    }

    return true;
  };
  let ketqua_mang = [];
  for (var i = 0; i < mang.length; i++) {
    if (checkSNT(mang[i])) {
      ketqua_mang.push(mang[i]);
    }
  }
  console.log(ketqua_mang);
};
console.log(
  "Câu 2 : Mảng : [3, 7, 2, 6, 14, 25, 16, 71, 83, 19, 1]. Xuất mảng số nguyên tố"
);
mangSNT([3, 7, 2, 6, 14, 25, 16, 71, 83, 19, 1]);

// Câu 3
let sum = (number) => {
  var s = 0;
  for (var i = 2; i <= number; i++) {
    s += i;
  }
  console.log(s + 2 * number);
};
console.log("Câu 3 : Tính tổng S = ( 2 + 3 + 4 ... + n ) + 2n, n = 21 ");
sum(21);

// Câu 4
let uocSo = (number) => {
  var dem = 0;
  for (var i = 1; i <= number; i++) {
    if (number % i == 0) {
      dem++;
    }
  }
  console.log(dem);
};
console.log("Câu 4 : Tìm các số là ước số n, n = 8 ");
uocSo(8);

// Câu 5
let swapSND = (number) => {
  var stringNumber = number.toString();
  var ketqua = "";
  for (var i = stringNumber.length - 1; i >= 0; i--) {
    ketqua += stringNumber[i];
  }
  console.log(ketqua);
};
console.log("Câu 5 : Đảo ngược số nguyên dương, n = 1234");
swapSND(1234);

// Câu 7
let bangCuuChuong = (number) => {
  for (let i = 1; i <= 10; i++) {
    console.log(number + " x " + i + " = " + number * i);
  }
};
console.log("Câu 7 : In bảng cửu chương, n = 7");
bangCuuChuong(7);
//
